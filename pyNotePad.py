# Copyright (c) 2023 Erdem Ersoy (eersoy93)
#
# Licensed with MIT license. See LICENSE file for full text.

import PySimpleGUI as psg
import sys

VERSION = "0.0.1"

def main(args):
    psg.theme("Default1")

    if len(args) > 1:
        try:
            f = open(args[1], "rt")
            f_text = f.read()
            f.close()
        except FileNotFoundError:
            psg.Popup("This file not found!", button_color="red3", custom_text="Ok!", no_titlebar=True)
        except PermissionError:
            psg.Popup("Access denied!", button_color="red3", custom_text="Ok!", no_titlebar=True)
        except UnicodeDecodeError:
            psg.Popup("Unicode decode error!", button_color="red3", custom_text="Ok!", no_titlebar=True)
    else:
        f_text = ""

    layout = [
        [psg.Text("Filename:"), psg.Input(key="-FILENAME-", expand_x=True)],
        [psg.Button("New", expand_x=True), psg.Button("Open", expand_x=True), psg.Button("Save", expand_x=True), psg.Button("Exit", expand_x=True)],
        [psg.Multiline(size=(80, 25), default_text=f_text, key="-TEXT-")]
    ]

    window = psg.Window(f"PyNotePad {VERSION}", layout)

    while True:
        event, values = window.read()
        if event == psg.WIN_CLOSED or event == "Exit":
            break
        if event == "New":
            window["-TEXT-"].update(value="")
        if event == "Open":
            try:
                with open(window["-FILENAME-"].get(), "rt") as f:
                    window["-TEXT-"].update(value=f.read())
                    f.close()
            except FileNotFoundError:
                psg.Popup("This file not found!", button_color="red3", custom_text="Ok!", no_titlebar=True)
            except PermissionError:
                psg.Popup("Access denied!", button_color="red3", custom_text="Ok!", no_titlebar=True)
            except UnicodeDecodeError:
                psg.Popup("Unicode decode error!", button_color="red3", custom_text="Ok!", no_titlebar=True)
        if event == "Save":
            try:
                with open(window["-FILENAME-"].get(), "wt") as f:
                    f.write(window["-TEXT-"].get())
                    f.close()
            except FileNotFoundError:
                psg.Popup("This file not found!", button_color="red3", custom_text="Ok!", no_titlebar=True)
            except PermissionError:
                psg.Popup("Access denied!", button_color="red3", custom_text="Ok!", no_titlebar=True)

    window.close()

if __name__ == "__main__":
    sys.exit(main(sys.argv))
